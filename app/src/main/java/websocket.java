import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.a0414831216.mypush.R;
import com.pusher.java_websocket.client.WebSocketClient;
import com.pusher.java_websocket.drafts.Draft;
import com.pusher.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 * Created by 0414831216 on 11/8/2018.
 */

public class websocket extends AppCompatActivity {
    private Button mButtonSubscribe;
    private Activity activity;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;

        mButtonSubscribe = (Button) findViewById(R.id.subscribeButton);
        mButtonSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    clientSocket clientSocket = new clientSocket(new URI("ws://192.168.250.14:8980/pushnotif/notif/admingo"));
                    clientSocket.connect();

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

            }
        });


    }



    private class clientSocket extends WebSocketClient{


        public clientSocket( URI serverUri , Draft draft ) {
            super( serverUri, draft );
        }

        public clientSocket( URI serverURI ) {
            super( serverURI );
        }

        public clientSocket( URI serverUri, Map httpHeaders ) {
            super(serverUri, (Draft) httpHeaders);
        }

        @Override
        public void connect() {
            super.connect();
            Toast.makeText(activity, "connect cooy",
                    Toast.LENGTH_LONG).show();
        }


        @Override
        public void onOpen(ServerHandshake handshakedata) {

        }

        @Override
        public void onMessage(String message) {

        }

        @Override
        public void onClose(int code, String reason, boolean remote) {

        }

        @Override
        public void onError(Exception ex) {
            Toast.makeText(activity, ex.getMessage(),
                    Toast.LENGTH_LONG).show();
        }

    }


}
